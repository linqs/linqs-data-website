'use strict';

let DATASETS;

function renderPage() {
    let datasetID = location.hash.trim().replace(/^#/, '');

    if (datasetID === '')
        makeStubDatasets();
    else {
        if (DATASETS.hasOwnProperty(datasetID)) {
            makeFullDataset(DATASETS[datasetID]);
        }
        else {
            makeStubDatasets();
        }
    }
}

function makeFullDataset(dataset) {
    let image = dataset['img'];
    let title = dataset['title'];
    let size = dataset['size'];
    let download = dataset['downloadLink'];
    let hash = dataset['md5'];

    let citation = dataset['citation'];
    let description = dataset['description'];

    let references = dataset['references'];
    let referencesHTML = '';

    references.forEach(function(reference, i) {
        let refLink = reference['link'];
        let refText = reference['text'];
        let referenceTemplate = `
            <p><a href='${refLink}'>${refText}</a></p>
        `;

        referencesHTML += referenceTemplate;
    });


    let templateString = `
        <div class='dataset-full'>
            <div class='top'>
                <div class='title'>
                    <h2>${title}</h2>
                </div>
                <div class='image'>
                    <img src='${image}'/>
                </div>
                <div class='stats'>
                    <div class='size'>
                        <label>Size</label>
                        <span>${size}</span>
                    </div>
                    <div class='download'>
                        <p><a href=${download}>Download</a></p>
                    </div>
                    <div class='hash'>
                        <label>md5</label>
                        <code>${hash}</code>
                    </div>
                </div>
            </div>
            <div class='citation'>
                <pre>
                    ${citation}
                </pre>
            </div>
            <div class='description'>
                ${description}
            </div>
            <div class='references'>
                ${referencesHTML}
            </div>
        </div>
    `;

    document.querySelector('.datasets').innerHTML = templateString;
}

function makeStubDatasets() {
    let stubs = '';


    Object.keys(DATASETS).forEach(function(key) {

        let dataset = DATASETS[key];

        let image = dataset['img'];
        let title = dataset['title'];
        let size = dataset['size'];
        let download = dataset['downloadLink'];
        let hash = dataset['md5'];
        let link = '#' + key;

        stubs += `
            <div class='dataset-stub'>
                <div class='top'>
                    <div class='title'>
                        <h2><a href='${link}'>${title}</a></h2>
                    </div>
                    <div class='image'>
                        <img src='${image}'/>
                    </div>
                    <div class='stats'>
                        <div class='size'>
                            <label>Size</label>
                            <span>${size}</span>
                        </div>
                        <div class='download'>
                            <p><a href=${download}>Download</a></p>
                        </div>
                        <div class='hash'>
                            <label>md5</label>
                            <code>${hash}</code>
                        </div>
                    </div>
                </div>
            </div>
        `;
    });

    document.querySelector('.datasets').innerHTML = stubs;
}

$().ready(function() {

    $(window).on('hashchange', function() {
        renderPage();
    });

    DATASETS = window.linqs.data.datasets;
    renderPage();
});
